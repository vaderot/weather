package com.vaderot.test.weather.api;

import com.vaderot.test.weather.R;
import com.vaderot.test.weather.WeatherApp;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Admin on 06.08.2015.
 */
public class ServerApi {

    public static String readInputStream(InputStream is, String charset) {
        final char[] buffer = new char[0x10000];
        try {
            StringBuilder out = new StringBuilder();
            Reader in = new InputStreamReader(is, charset);
            int read;
            do {
                read = in.read(buffer, 0, buffer.length);
                if (read > 0) {
                    out.append(buffer, 0, read);
                }
            } while (read >= 0);
            return out.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getWeather(String city){
        String reqest = WeatherApp.getApplication().getResources().getString(R.string.weather_request);
        String result = "";

        reqest += city.replace(" ", "%20");//rep;ase space for GET

        URL url;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL(reqest);

            urlConnection = (HttpURLConnection) url
                    .openConnection();
            InputStream ips = urlConnection.getInputStream();
            result = readInputStream(ips, "UTF-8");

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                urlConnection.disconnect();
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
