package com.vaderot.test.weather.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.vaderot.test.weather.Consts;
import com.vaderot.test.weather.R;
import com.vaderot.test.weather.WeatherApp;
import com.vaderot.test.weather.model.openWeatherJson.WeatherResponse;
import com.vaderot.test.weather.widgets.ImageLoadView;

import java.util.Date;

/**
 * Created by Admin on 05.08.2015.
 */
public class CityWeatherFragment extends Fragment {

    private final String mImgSourse = WeatherApp.getApplication().getResources().getString(R.string.weather_icon_source);

    private WeatherResponse mWeatherResponse;

    private ImageLoadView mIconWeather;
    private TextView mCityName;
    private TextView mTemperature;
    private TextView mWind;
    private TextView mCloudiness;
    private TextView mCPressure;
    private TextView mHumidity;
    private TextView mSunrise;
    private TextView mSunset;
    private TextView mGeoCoords;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_city_weather, null);

        mIconWeather = (ImageLoadView) view.findViewById(R.id.iconWeather);
        mCityName = (TextView) view.findViewById(R.id.cityName);
        mTemperature = (TextView) view.findViewById(R.id.temperature);
        mWind = (TextView) view.findViewById(R.id.wind);
        mCloudiness = (TextView) view.findViewById(R.id.cloudiness);
        mCPressure = (TextView) view.findViewById(R.id.pressure);
        mHumidity = (TextView) view.findViewById(R.id.humidity);
        mSunrise = (TextView) view.findViewById(R.id.sunrise);
        mSunset = (TextView) view.findViewById(R.id.sunset);
        mGeoCoords = (TextView) view.findViewById(R.id.geoCoords);

        if(savedInstanceState != null) {
            String weathrRespose = savedInstanceState.getString(Consts.WEATHER_RESPONSE, "-1");
            if (weathrRespose != null && !weathrRespose.equals("-1")) {
                mWeatherResponse = new Gson().fromJson(weathrRespose, WeatherResponse.class);
                init();
            }
        }

        return view;
    }

    private void init(){
        mIconWeather.load(String.format(mImgSourse, mWeatherResponse.getWeather()[0].getIcon()));
        mCityName.setText(mWeatherResponse.getCity() + ", " + mWeatherResponse.getSys().getCountry());
        mTemperature.setText(String.format("%.1f", mWeatherResponse.getMain().getTemperature() - 273.15) + getString(R.string.celsius));
        mWind.setText(mWeatherResponse.getWind().getSpeed() + getString(R.string.meters_per_second) + windDirection(mWeatherResponse.getWind().getDeg()) + " (" + mWeatherResponse.getWind().getDeg() + ")");
        mCloudiness.setText(mWeatherResponse.getWeather()[0].getDescription() + ", " + mWeatherResponse.getClouds().getAll() + "%");
        mCPressure.setText(mWeatherResponse.getMain().getPressure() + getString(R.string.pascal));
        mHumidity.setText(mWeatherResponse.getMain().getHumidity() + " %");
        mSunrise.setText(Consts.timeSimpleDateFormat.format(new Date(mWeatherResponse.getSys().getSunrise() * 1000)));
        mSunset.setText(Consts.timeSimpleDateFormat.format(new Date(mWeatherResponse.getSys().getSunset() * 1000)));
        mGeoCoords.setText(String.format("[%.2f, %.2f]", mWeatherResponse.getCoord().getLat(), mWeatherResponse.getCoord().getLon()));
    }

    public void setWeatherResponse(WeatherResponse weatherResponse){
        mWeatherResponse = weatherResponse;
        init();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mWeatherResponse != null)
            outState.putString(Consts.WEATHER_RESPONSE, new Gson().toJson(mWeatherResponse));
    }

    private String windDirection(float windDeg){
        int windDirection = (int)((windDeg + 11.25) / 22.5 % 16);
        return getResources().getStringArray(R.array.wind_direction)[windDirection];
    }



}
