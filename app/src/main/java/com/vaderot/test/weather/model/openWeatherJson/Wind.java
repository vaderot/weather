package com.vaderot.test.weather.model.openWeatherJson;

/**
 * Created by Admin on 05.08.2015.
 */
public class Wind {

    private float speed;// Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
    private float gust;// Wind gust. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
    private float deg;// Wind direction, degrees (meteorological)

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getGust() {
        return gust;
    }

    public void setGust(float gust) {
        this.gust = gust;
    }

    public float getDeg() {
        return deg;
    }

    public void setDeg(float deg) {
        this.deg = deg;
    }
}
