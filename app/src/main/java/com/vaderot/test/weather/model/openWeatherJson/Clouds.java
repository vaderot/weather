package com.vaderot.test.weather.model.openWeatherJson;

/**
 * Created by Admin on 05.08.2015.
 */
public class Clouds {
    private int all;//Cloudiness, %

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
