package com.vaderot.test.weather.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.vaderot.test.weather.R;
import com.vaderot.test.weather.api.ServerApi;
import com.vaderot.test.weather.fragments.CityWeatherFragment;
import com.vaderot.test.weather.model.openWeatherJson.WeatherResponse;

import java.net.InetAddress;


public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private CityWeatherFragment mCityWeatherFragment;
    private SupportMapFragment mMapFragment;
    GoogleMap mGoogleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        mCityWeatherFragment = (CityWeatherFragment) fragmentManager.findFragmentById(R.id.weatherFragment);
        mMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.mapFragment);
        mGoogleMap = mMapFragment.getMap();

        setToolBar();
    }

    private void setToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.actionBar);
        setSupportActionBar(toolbar);
        SearchView searchView = (SearchView) findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String city) {
                if (!city.equals("")) {
                    if(isNetworkAvailable())
                        new WeatherRequestTask().execute(city);
                    else
                        Toast.makeText(MainActivity.this, "nnNo Internet connection", Toast.LENGTH_SHORT).show();
                }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

//    There is a possibility it's connected to a network but not to internet
    private boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");

            if (ipAddr.equals("")) {
                return false;
            } else {
                return true;
            }

        } catch (Exception e) {
            return false;
        }
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private class WeatherRequestTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            if(isInternetAvailable())
                return ServerApi.getWeather(params[0]);
            else
                return null;

        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if(response == null) {
                Toast.makeText(MainActivity.this, "No Internet connection", Toast.LENGTH_SHORT).show();
                return;
            }

            WeatherResponse weatherResponse = new Gson().fromJson(response, WeatherResponse.class);
            if(weatherResponse.getCode() == 404){
                Toast.makeText(MainActivity.this, weatherResponse.getMessage(), Toast.LENGTH_SHORT).show();
                return;
            }
            mCityWeatherFragment.setWeatherResponse(weatherResponse);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(weatherResponse.getCoord().getLat(), weatherResponse.getCoord().getLon()), 10);
            mGoogleMap.animateCamera(cameraUpdate);
        }
    }
}
