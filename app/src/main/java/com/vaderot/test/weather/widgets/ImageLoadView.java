package com.vaderot.test.weather.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Admin on 06.08.2015.
 */
public class ImageLoadView  extends ImageView {


    public ImageLoadView(Context context) {
        super(context);
    }

    public ImageLoadView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageLoadView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void load(String url) {
        ImageView avatarImage = this;
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoader.getInstance().displayImage(url, avatarImage, defaultOptions);
    }
}