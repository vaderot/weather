package com.vaderot.test.weather;

import java.text.SimpleDateFormat;

/**
 * Created by Admin on 06.08.2015.
 */
public class Consts {

    public static final String WEATHER_RESPONSE = "weatherResponse";

    public static final SimpleDateFormat timeSimpleDateFormat = new SimpleDateFormat("HH:mm");
}
