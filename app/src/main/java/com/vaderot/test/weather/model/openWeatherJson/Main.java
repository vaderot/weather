package com.vaderot.test.weather.model.openWeatherJson;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 05.08.2015.
 */
public class Main {

    @SerializedName("temp")
    private float temperature;// Temperature. Celsius
    private float pressure;// Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
    private int humidity;// Humidity, %

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }


}
