package com.vaderot.test.weather;

import android.app.Application;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by Admin on 06.08.2015.
 */
public class WeatherApp extends Application {

    private static WeatherApp sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;

        initImageLoader();

    }

    private void initImageLoader(){
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(1)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .diskCacheFileCount(50)
                .memoryCacheSize(3 * 1024 * 1024)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheExtraOptions(480, 320, null)
                .build();

        ImageLoader.getInstance().init(config);
    }

    public static WeatherApp getApplication(){
        return sContext;
    }
}
