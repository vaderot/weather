package com.vaderot.test.weather.model.openWeatherJson;

/**
 * Created by Admin on 05.08.2015.
 */
public class Coord {
    private float lat;//City geo location, latitude
    private float lon;//City geo location, longitude

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }
}
