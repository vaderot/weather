Test project for the interview


Тесhnical Assignment:

Weather (Mobile) 
“Weather is the state of the atmosphere, to the degree that it is hot or cold, wet or dry, calm or 
stormy, clear or cloudy. Weather, seen from an anthropological perspective, is something all 
humans in the world constantly experience through their senses, at least while being outside. 
There are socially and scientifically constructed understandings of what weather is, what makes it 
change, the effect it has on humans in different situations, etc. Therefore weather is something 
people often communicate about. Turning back to the meteorological perspective, most weather 
phenomena occur in the troposphere, just below the stratosphere. Weather generally refers to day-
to-day temperature and precipitation activity, whereas climate is the term for the statistics of 
atmospheric conditions over longer periods of time. When used without qualification, "weather" is 
generally understood to mean the weather of Earth.”  
(From “Weather” article on Wikipedia ­ http://en.wikipedia.org/wiki/Weather)  

Your task is to implement a simple mobile app (Android) for users to interact with weather web 
service (http://openweathermap.org/current) providing weather data for different cities. The user 
will write the name of the city in field in Action Bar, after get data from server and display all 
received information. 

Web service
● Root URL: http://api.openweathermap.org/
● Format: JSON (application/json)
● Endpoints:
○ data/2.5/weather/?q=London ­ weather information for London
○ img/w/01d.png ­ get weather state icon

See example : http://openweathermap.org/current

Requirements UI:
 Display city on map
 Display all weather information + icon
 Minimum Android 4.0.3 (15 API)